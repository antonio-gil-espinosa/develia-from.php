<?php

namespace Develia;


/**
 * @template T
 * @param $source
 * @return From
 * @throws \Exception
 */
class From implements \IteratorAggregate, \Countable
{


    /**
     * @var callable():iterable<T>
     */
    private $generator_fn;

    private function __construct() {}

    /**
     *
     * @param $source callable():iterable<T>
     * @return From<T>
     */
    public static function fn($source)
    {
        $output = new From();
        $output->generator_fn = $source;
        return $output;

    }

    /**
     * @return From
     * @throws \Exception
     */
    public static function iterable($source)
    {
        $output = new From();
        $output->generator_fn = function () use ($source) {
            return From::_getGenerator($source);
        };
        return $output;
    }

    /**
     * @param array $keys
     * @param array $values
     * @return From
     * @throws \Exception
     */
    public static function keysValues(array $keys, array $values)
    {
        return From::fn(function () use ($values, $keys) {

            for ($i = 0; $i < count($keys); $i++)
                yield $keys[$i] => $values[$i];

        });

    }

    private static function _keyValueIndexIdentityFn($k, $v, $i)
    {
        return $v;
    }

    private static function _identityFn($v)
    {
        return $v;
    }

    /**
     * @param $func
     * @return \Closure|null
     * @throws \ReflectionException
     * @noinspection PhpUnusedParameterInspection
     */
    private static function _getKeyValueIndexFn($func)
    {
        if (is_null($func))
            return null;

        /** @noinspection PhpUnhandledExceptionInspection */
        $info = new \ReflectionFunction($func);
        $num = $info->getNumberOfParameters();

        switch ($num) {
            case 0:
                return function ($k, $v, $i) use ($func) {
                    return call_user_func($func);
                };
            case 1:
                return function ($k, $v, $i) use ($func) {
                    return call_user_func($func, $v);
                };
            case 2:
                return function ($k, $v, $i) use ($func) {
                    return call_user_func($func, $k, $v);
                };
            case 3:
                return function ($k, $v, $i) use ($func) {
                    return call_user_func($func, $k, $v, $i);
                };
            default:
                throw new \InvalidArgumentException("Invalid argument number in callable");
        }


    }

    /**
     * @param \Traversable | array $source
     * @return \Generator
     * @throws \Exception
     */
    private static function _getGenerator($source)
    {
        foreach ($source as $k => $v) {
            yield $k => $v;
        }
    }

    public function splice($predicate, $getter)
    {
        return From::fn(function () use ($getter, $predicate) {
            $predicate = From::_getKeyValueIndexFn($predicate);
            $getter = From::_getKeyValueIndexFn($getter);
            $i = 0;
            foreach ($this->getIterator() as $k => $v) {
                if (call_user_func($predicate, $k, $v, $i++)) {
                    foreach (call_user_func($getter, $k, $v, $i) as $k2 => $v2)
                        yield $k2 => $v2;
                } else {
                    yield $k => $v;
                }

            }
        });
    }

    /**
     * Checks if all elements in the iterable are instances of a specified class.
     *
     * Iterates over each element in the iterable. If an element is found that is not an instance of the specified class,
     * the function returns false. If all elements are instances of the specified class, the function returns true.
     *
     * @param iterable $iterable The iterable to check.
     * @param string $class The name of the class to check instances against.
     *
     * @return bool Returns true if all elements in the iterable are instances of the specified class, otherwise false.
     */
    public function allInstancesOf($class)
    {
        foreach ($this as $element) {
            if (!($element instanceof $class)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Yields instances of a specific class from an iterable.
     *
     * Iterates over each element in the iterable. If an element is an instance of the specified class,
     * it is yielded.
     * @template class-string
     * @param class-string $class The name of the class to check instances against.
     * @return From<class-string> Yields instances of the specified class.
     */
    public function instancesOf($class) {}

    /**
     * @param string $separator
     * @return string
     */
    function implode($separator)
    {
        $output = "";


        foreach ($this as $item) {
            if ($output == "")
                $output = strval($item);
            else
                $output .= $separator . $item;
        }
        return $output;
    }

    /** @noinspection PhpUnusedParameterInspection */

    /**
     * @param mixed $key
     * @param mixed $output
     * @param mixed $fallback
     * @return bool
     */
    public function tryGet($key, &$output, $fallback = null)
    {

        foreach ($this as $k => $v) {
            if ($k == $key) {
                $output = $v;
                return true;
            }
        }

        if (func_num_args() > 2)
            $output = $fallback;

        return false;
    }

    /**
     * @param $key
     * @param $output
     * @param $fallback
     * @return bool
     * @deprecated Use tryGet
     */
    public function tryGetElementByKey($key, &$output, $fallback = null)
    {
        foreach ($this as $k => $v) {
            if ($k == $key) {
                $output = $v;
                return true;
            }
        }

        if (func_num_args() > 2)
            $output = $fallback;

        return false;
    }

    /**
     * @param $group_by_selector
     * @param $aggregation
     * @return From
     * @throws \Exception
     */
    public function groupBy($group_by_selector, $aggregation = null)
    {
        return From::fn(function () use ($aggregation, $group_by_selector) {


            $group_by_selector = self::_getKeyValueIndexFn($group_by_selector);
            $aggregation = self::_getKeyValueIndexFn($aggregation);

            $keys = [];
            $values = [];

            $i = 0;
            foreach ($this as $k => $v) {
                $key = call_user_func($group_by_selector, $k, $v, $i);
                if (!in_array($key, $keys))
                    $keys[] = $key;

                $index = array_search($key, $keys);
                if (!isset($values[$index]))
                    $values[$index] = [];

                $values[$index][$k] = $v;
                $i++;
            }

            if ($aggregation)
                for ($i = 0; $i < count($keys); $i++) {
                    yield $keys[$i] => call_user_func($aggregation, $keys[$i], From::iterable($values[$i]), $i);
                }
            else {
                for ($i = 0; $i < count($keys); $i++) {
                    yield $keys[$i] => From::iterable($values[$i]);
                }
            }

        });
    }

    /**
     * @throws \Exception
     */
    public function collect()
    {


        $keys = [];
        $values = [];

        foreach ($this as $k => $v) {
            $keys[] = $k;
            $values[] = $v;
        }

        return From::keysValues($keys, $values);

    }

    /**
     * @return int
     * @throws \Exception
     */
    public function count($predicate = null)
    {
        if ($predicate) {
            return $this->filter($predicate)->count();
        } else {
            $output = 0;
            foreach ($this->getIterator() as $ignored) {
                $output++;
            }
            return $output;
        }

    }

    /**
     * @param $predicate
     * @return From<T>
     * @throws \Exception
     */
    public function filter($predicate)
    {
        return From::fn(function () use ($predicate) {
            $i = 0;
            $predicate = From::_getKeyValueIndexFn($predicate);
            foreach ($this->getIterator() as $k => $v) {
                if (call_user_func($predicate, $k, $v, $i++)) {
                    yield $k => $v;
                }

            }
        });
    }

    /**
     * @return \Iterator
     */
    public function getIterator()
    {
        return call_user_func($this->generator_fn);
    }

    public function reduce($func)
    {
        $iterator = $this->getIterator();
        $iterator->rewind();

        $output = $iterator->current();
        $iterator->next();

        while ($iterator->valid()) {
            $output = call_user_func($func, $output, $iterator->current());
            $iterator->next();
        }

        return $output;
    }

    /**
     * @param null | callable $selector
     * @return float|int
     * @throws \ReflectionException
     */
    public function average($selector = null)
    {
        $selector = $selector ? From::_getKeyValueIndexFn($selector) : [self::class, "_keyValueIndexIdentityFn"];

        $output = 0;
        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            $output += call_user_func($selector, $k, $v, $i);
            $i++;
        }
        return $output / $i;
    }

    /**
     * @param null | callable $selector
     * @return float|int
     * @throws \ReflectionException
     */
    public function sum($selector = null)
    {
        $selector = $selector ? From::_getKeyValueIndexFn($selector) : [self::class, "_keyValueIndexIdentityFn"];

        $output = 0;
        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            $output += call_user_func($selector, $k, $v, $i);
            $i++;
        }
        return $output;
    }

    /**
     * @throws \Exception
     */
    public function union($transversable)
    {
        return From::fn(function () use ($transversable) {
            foreach ($this->getIterator() as $item)
                yield $item;
            foreach ($transversable as $item)
                yield $item;
        });
    }

    /**
     * @param $predicate | null
     * @return bool
     * @throws \ReflectionException
     */
    public function any($predicate = null)
    {
        $predicate = From::_getKeyValueIndexFn($predicate ?: function () {
            return true;
        });

        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            if (call_user_func($predicate, $k, $v, $i++))
                return true;
        }

        return false;
    }

    /**
     * @param $predicate
     * @return bool
     * @throws \ReflectionException
     */
    public function all($predicate)
    {

        $predicate = From::_getKeyValueIndexFn($predicate);

        $i = 0;
        foreach ($this->getIterator() as $k => $v) {
            if (!call_user_func($predicate, $k, $v, $i++))
                return false;
        }

        return true;
    }

    /**
     * @template class-string
     * @param class-string $class
     * @return From<class-string>
     * @noinspection PhpUnusedParameterInspection
     */
    public function classHint($class)
    {
        return self::fn([$this, "getIterator"]);

    }

    /**
     * @template R
     * @param callable(T):R $selector
     * @return From<R>
     */
    public function map($selector)
    {
        return From::fn(function () use ($selector) {

            $i = 0;
            $selector = From::_getKeyValueIndexFn($selector);

            foreach ($this->getIterator() as $k => $v) {
                yield $k => call_user_func($selector, $k, $v, $i++);
            }

        });
    }

    /**
     * @param $selector
     * @return From
     * @throws \Exception
     */
    public function mapKV($selector)
    {


        return From::fn(function () use ($selector) {

            $i = 0;
            $selector = From::_getKeyValueIndexFn($selector);

            foreach ($this->getIterator() as $k => $v) {
                list($key, $value) = call_user_func($selector, $k, $v, $i++);
                yield $key => $value;
            }

        });
    }

    /**
     * @param $selector
     * @return From
     * @throws \Exception
     */
    public function mapMany($selector)
    {

        return From::fn(function () use ($selector) {

            $i = 0;
            $selector = From::_getKeyValueIndexFn($selector);

            foreach ($this->getIterator() as $k => $v) {
                foreach (call_user_func($selector, $k, $v, $i) as $subitem) {
                    yield $subitem;
                }
            }
        });
    }

    /**
     * @param $selector
     * @return mixed
     * @throws \ReflectionException
     */
    public function max($selector = null)
    {
        /** @noinspection DuplicatedCode */
        $selector = $selector ? From::_getKeyValueIndexFn($selector) : [self::class, "_keyValueIndexIdentityFn"];

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $current = $iterator->current();
        $output = call_user_func($selector, $key, $current, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $key = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $key, $current, $i++);

            if ($value > $output)
                $output = $value;

            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $selector
     * @return mixed
     * @throws \ReflectionException
     */
    public function min($selector = null)
    {

        /** @noinspection DuplicatedCode */
        $selector = $selector ? From::_getKeyValueIndexFn($selector) : [self::class, "_keyValueIndexIdentityFn"];

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $current = $iterator->current();
        $output = call_user_func($selector, $key, $current, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $key = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $key, $current, $i++);

            if ($value < $output)
                $output = $value;

            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $n
     * @return From
     * @throws \Exception
     */
    public function take($n)
    {
        return From::fn(function () use ($n) {

            $i = 0;
            foreach ($this->getIterator() as $item) {
                if ($n > $i) {
                    yield $item;
                    $i++;
                } else {
                    break;
                }
            }

        });
    }

    /**
     * @param $selector
     * @return mixed
     * @throws \ReflectionException
     */
    public function highest($selector)
    {

        $selector = From::_getKeyValueIndexFn($selector);

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $key = $iterator->key();
        $output = $iterator->current();
        $max_value = call_user_func($selector, $key, $output, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $k = $iterator->key();
            $current = $iterator->current();
            $value = call_user_func($selector, $k, $current, $i++);

            if ($value > $max_value) {
                $output = $current;
                $max_value = $value;
            }


            $iterator->next();
        }

        return $output;

    }

    /**
     * @param $item
     * @return From
     * @throws \Exception
     */
    public function without($item)
    {
        return $this->filter(function ($x) use ($item) {
            return $x != $item;
        });
    }

    /**
     * @param $selector
     * @param $order
     * @return From
     * @throws \Exception
     */
    public function orderBy($selector, $order = "asc")
    {

        return From::fn(function () use ($selector, $order) {


            $copy = $this->toMap();

            if (strtolower($order) == "desc") {
                usort($copy, function ($a, $b) use ($selector) {
                    return call_user_func($selector, $a) < call_user_func($selector, $b);
                });

            } else if (strtolower($order) == "asc") {
                usort($copy, function ($a, $b) use ($selector) {
                    return call_user_func($selector, $a) > call_user_func($selector, $b);
                });
            }

            foreach ($copy as $item)
                yield $item;

        });
    }

    /**
     * @return array
     */
    public function toMap()
    {
        return iterator_to_array($this->getIterator());
    }

    /**
     * @param $callable
     * @return void
     * @throws \ReflectionException
     */
    public function each($callable)
    {
        $callable = From::_getKeyValueIndexFn($callable);
        $i = 0;
        foreach ($this->getIterator() as $k => $v)
            call_user_func($callable, $k, $v, $i++);
    }

    /**
     * @return From
     * @throws \Exception
     */
    public function flatten()
    {
        return From::fn(function () {
            foreach ($this->getIterator() as $item) {
                foreach ($item as $subitem)
                    yield $subitem;
            }
        });
    }

    /**
     * @param $selector
     * @return mixed
     * @throws \ReflectionException
     */
    public function lowest($selector)
    {

        $function = From::_getKeyValueIndexFn($selector);

        $iterator = $this->getIterator();
        $iterator->rewind();

        $i = 0;
        $output = $iterator->current();
        $min_value = call_user_func($function, $iterator->current(), $output, $i++);

        $iterator->next();

        while ($iterator->valid()) {

            $k = $iterator->key();
            $obj = $iterator->current();
            $value = call_user_func($function, $k, $obj, $i++);

            if ($value < $min_value) {
                $output = $obj;
                $min_value = $value;
            }


            $iterator->next();
        }

        return $output;
    }

    /**
     * @return From
     * @throws \Exception
     */
    public function keys()
    {
        return From::fn(function () {
            foreach ($this as $k => $v) {
                yield $k;
            }
        });

    }

    /**
     * @return From
     * @throws \Exception
     */
    public function values()
    {
        return From::fn(function () {
            foreach ($this as $v) {
                yield $v;
            }
        });
    }

    /**
     * @param $obj
     * @return bool
     */
    public function contains($obj)
    {
        foreach ($this as $item) {
            if ($obj === $item) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $key
     * @return bool
     */
    public function containsKey($key)
    {
        foreach ($this as $k => $v) {
            if ($key === $k) {
                return true;
            }
        }
        return false;
    }


    /**
     * @param string|array|callable $predicate
     * @param From<T> $ifTrue
     * @param From<T> $ifFalse
     * @return void
     * @throws \ReflectionException
     */
    public function split($predicate, &$ifTrue, &$ifFalse)
    {
        $predicate = self::_getKeyValueIndexFn($predicate);

        $ifTrueTmp = [];
        $ifFalseTmp = [];

        $i = 0;
        foreach ($this as $k => $v) {
            if (call_user_func($predicate, $k, $v, $i))
                $ifTrueTmp[$k] = $v;
            else
                $ifFalseTmp[$k] = $v;

            $i++;
        }

        $ifTrue = From::iterable($ifTrueTmp);
        $ifFalse = From::iterable($ifFalseTmp);
    }

    /**
     * @param mixed|null $fallback
     * @return T
     */
    public function first($predicate = null, $fallback = null)
    {
        $predicate = self::_getKeyValueIndexFn($predicate);

        if ($predicate) {
            $i = 0;
            foreach ($this->getIterator() as $k => $v) {
                if (call_user_func($predicate, $k, $v, $i))
                    return $v;
                $i++;
            }

        } else {
            foreach ($this->getIterator() as $item)
                return $item;
        }
        return $fallback;
    }

    /**
     * @param $callable
     * @return From
     * @throws \Exception
     */
    function tap($callable)
    {
        return From::fn(function () use ($callable) {
            $callable = self::_getKeyValueIndexFn($callable);
            $i = 0;
            foreach ($this as $k => $v) {
                call_user_func($callable, $k, $v, $i++);
                yield $k => $v;
            }
        });

    }

    /**
     * @param $condition
     * @param callable $callable
     * @return From
     */
    function conditional($condition, $callable)
    {
        if ($condition)
            return call_user_func($callable, $this);
        return $this;
    }

    /**
     * @param $i
     * @return mixed
     */
    public function elementAt($i)
    {
        $j = 0;
        foreach ($this->getIterator() as $item) {
            if ($i == $j)
                return $item;

            $j++;
        }
        throw new \OutOfRangeException();

    }

    /**
     * @throws \Exception
     */
    function unique()
    {
        return From::fn(function () {
            foreach (array_unique($this->toList()) as $item)
                yield $item;
        });

    }

    /**
     * @return array
     */
    public function toList()
    {
        return iterator_to_array($this->getIterator(), false);
    }

    /**
     * @param $objs
     * @return bool
     */
    public function containsAny($objs)
    {
        foreach ($this->getIterator() as $item) {
            if (in_array($item, $objs)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $iterable
     * @return From
     * @throws \Exception
     */
    public function intersect($iterable)
    {

        return From::fn(function () use ($iterable) {

            $array1 = iterator_to_array($this->getIterator());
            $array2 = iterator_to_array(self::_getGenerator($iterable));

            foreach (array_intersect($array1, $array2) as $item)
                yield $item;

        });
    }

    /**
     * @param $obj
     * @return From
     * @throws \Exception
     */
    public function append($obj, $key = null)
    {
        return From::fn(function () use ($key, $obj) {
            if (is_null($key)) {
                foreach ($this->getIterator() as $k => $v)
                    yield $k => $v;

                yield $obj;
            } else {
                foreach ($this->getIterator() as $k => $v)
                    yield $k => $v;

                yield $key => $obj;
            }

        });
    }

    /**
     * @param $obj
     * @return From
     * @throws \Exception
     */
    public function prepend($obj, $key = null)
    {
        return From::fn(function () use ($key, $obj) {
            if (is_null($key)) {

                yield $obj;
                foreach ($this->getIterator() as $k => $v)
                    yield $k => $v;

            } else {

                yield $key => $obj;
                foreach ($this->getIterator() as $k => $v)
                    yield $k => $v;

            }

        });
    }

    /**
     * @param $n
     * @return From
     * @throws \Exception
     */
    public function skip($n)
    {
        return From::fn(function () use ($n) {

            $skipped = 0;
            foreach ($this->getIterator() as $k => $v) {
                if ($skipped < $n) {
                    $skipped++;
                } else {
                    yield $k => $v;
                }
            }


        });

    }

    /**
     * @param iterable $iterable
     * @param callable $on
     * @param callable|null $selector
     * @return From
     * @throws \Exception
     */
    public function join($iterable, callable $on, callable $selector = null)
    {
        $selector = $selector ?: function ($a, $b) {
            return [$a, $b];
        };

        return From::fn(function () use ($selector, $on, $iterable) {

            foreach ($this->getIterator() as $item1) {
                foreach ($iterable as $item2) {
                    if (call_user_func($on, $item1, $item2)) {
                        yield call_user_func($selector, $item1, $item2);
                    }
                }
            }

        });
    }

    /**
     * @param iterable $iterable
     * @param callable $on
     * @param callable|null $selector
     * @return From
     * @throws \Exception
     */
    public function leftJoin($iterable, callable $on, callable $selector = null)
    {
        $selector = $selector ?: function ($a, $b) {
            return [$a, $b];
        };

        return From::fn(function () use ($selector, $on, $iterable) {

            foreach ($this->getIterator() as $item1) {
                $matched = false;
                foreach ($iterable as $item2) {
                    if (call_user_func($on, $item1, $item2)) {
                        yield call_user_func($selector, $item1, $item2);
                        $matched = true;
                    }
                }
                if (!$matched) {
                    yield call_user_func($selector, $item1, null);
                }
            }

        });
    }

    /**
     * @param iterable $iterable
     * @param callable $on
     * @param callable|null $selector
     * @return From
     * @throws \Exception
     */
    public function rightJoin($iterable, callable $on, callable $selector = null)
    {
        return from($iterable)->leftJoin($this, $on, $selector);
    }
}

