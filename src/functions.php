<?php


if (!function_exists("from")) {


    /**
     * @template T
     * @param iterable<T> $source
     * @return \Develia\From<T>
     * @throws Exception
     */
    function from($source)
    {
        return \Develia\From::iterable($source);
    }
    

}


