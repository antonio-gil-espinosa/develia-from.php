<?php

require_once("../src/From.php");

use Develia\From;
use PHPUnit\Framework\TestCase;

class FromTest extends TestCase
{


    public function testGroupBy()
    {
        $array = [[
                      "grupo" => [1],
                      "valor" => "11"
                  ],
                  [
                      "grupo" => [1],
                      "valor" => "12"
                  ],
                  ["grupo" => [2],
                   "valor" => "21"
                  ],
                  ["grupo" => [2],
                   "valor" => "22",
                  ]];

        $grouping = From::iterable($array)->groupBy(function ($x) {
            return ["grupo" => $x["grupo"]];
        }, function (From $x) {
            return $x->toList();
        });

        $keys = $grouping->keys()->toList();
        $values = $grouping->values()->toList();

        $this->assertEquals([["grupo" => [1]], ["grupo" => [2]]], $keys);
        $this->assertEquals([[[
                                  "grupo" => [1],
                                  "valor" => "11"
                              ],
                              [
                                  "grupo" => [1],
                                  "valor" => "12"
                              ]],
                             [["grupo" => [2],
                               "valor" => "21"
                              ],
                              ["grupo" => [2],
                               "valor" => "22",
                              ]]], $values);


        $this->assertEquals(From::iterable($array)->groupBy(function ($x) {
            return $x["grupo"];
        }, function ($x) {
            return $x->sum(function ($y) {
                return $y["valor"];
            });
        })->toList(),
            [23, 43]);


    }

    public function testMap()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from1 = $from->map(function ($x) {
            return $x * 2;
        });
        $this->assertEquals([2, 4, 6, 8], $from1->toMap());

    }

    public function testUnion()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from1 = $from->union([1, 2, 3, 4]);
        $this->assertEquals([1, 2, 3, 4, 1, 2, 3, 4], $from1->toMap());
    }

    public function testAverage()
    {
        $array = [1, 2];
        $from = From::iterable($array);
        $this->assertEquals(1.5, $from->average());
    }


    public function testSkip()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from1 = $from->skip(2);
        $this->assertEquals([3, 4], $from1->toList());
    }


    public function testIntersect()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from1 = $from->intersect([4, 3]);
        $this->assertEquals([3, 4], $from1->toMap());
    }


    public function testFilter()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from2 = $from->filter(function ($k, $x) {
            return $x > 2;
        });
        $this->assertEquals([3, 4], $from2->toList());

        $from1 = $from->filter(function ($k, $x, $i) {
            return $x > 2;
        });
        $this->assertEquals([3, 4], $from1->toList());

    }


    public function testOrderBy()
    {
        $array = [1, 3, 2, 4];
        $from = From::iterable($array);
        $from2 = $from->orderBy(function ($x) {
            return $x;
        }, "desc");
        $this->assertEquals([4, 3, 2, 1], $from2->toMap());

        $from1 = $from->orderBy(function ($x) {
            return $x;
        }, "asc");
        $this->assertEquals([1, 2, 3, 4], $from1->toMap());
    }

    public function testAny()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $this->assertEquals(true, $from->any());
        $this->assertEquals(false,
            $from->any(function ($x) {
                return $x > 4;
            }));
    }

    public function testContainsAny()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $this->assertEquals(true, $from->containsAny([3, 5]));
        $this->assertEquals(false, $from->containsAny([-3, 5]));

    }


    public function testMapMany()
    {
        $array = [[1, 2], [2, 3]];
        $from = From::iterable($array);
        $from1 = $from->mapMany(function ($x) {
            return $x;
        });
        $this->assertEquals([1, 2, 2, 3], $from1->toMap());
    }

    public function testKeys()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from1 = $from->keys()->toList();
        $this->assertEquals([0, 1, 2, 3], $from1);
    }

    public function testHigest()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $this->assertEquals(4,
            $from->highest(function ($x) {
                return $x * 2;
            }));
    }

    public function testContains()
    {
        $array = [1, 2, 22, 4];
        $from = From::iterable($array);
        $this->assertEquals(true, $from->contains(22));
        $this->assertEquals(false, $from->contains(33));
    }

    public function testTake()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $from1 = $from->take(2);
        $this->assertEquals([1, 2], $from1->toMap());
    }

    public function testCount()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $this->assertEquals(4, $from->count());
        $this->assertEquals(2,
            $from->count(function ($x) {
                return $x > 2;
            }));
    }

    public function testToArray()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $this->assertEquals([1, 2, 3, 4], $from->toMap());
    }

    public function testPrepend()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $from1 = $from->prepend(5);
        $this->assertEquals([5, 1, 2, 3, 4], $from1->toList());


        $array = ["x" => 1, "y" => 2, "z" => 3];
        $from = From::iterable($array);

        $from1 = $from->prepend(4, "a");
        $this->assertEquals(["a" => 4, "x" => 1, "y" => 2, "z" => 3], $from1->toMap());
    }

    public function testMax()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $this->assertEquals(4, $from->max());
    }

    public function testAppend()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $from1 = $from->append(5);
        $this->assertEquals([1, 2, 3, 4, 5], $from1->toMap());

        $array = ["x" => 1, "y" => 2, "z" => 3];
        $from = From::iterable($array);

        $from1 = $from->append(4, "a");
        $this->assertEquals(["a" => 4, "x" => 1, "y" => 2, "z" => 3], $from1->toMap());
    }

    public function testMin()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $this->assertEquals(1, $from->min());
    }

    public function testUnique()
    {
        $array = [1, 2, 2, 3, 4];
        $from = From::iterable($array);

        $from1 = $from->unique();
        $this->assertEquals([1, 2, 3, 4], $from1->toMap());
    }

    public function testValues()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $from1 = $from->values()->toList();
        $this->assertEquals([1, 2, 3, 4], $from1);
    }

    public function testAll()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $this->assertEquals(true,
            $from->all(function ($x) {
                return $x > 0;
            }));

        $this->assertEquals(false,
            $from->all(function ($x) {
                return $x < 0;
            }));
    }

    public function testFlatten()
    {
        $array = [[1, 2], [2, 3], [3, 4], [4, 5]];
        $from = From::iterable($array);

        $from1 = $from->flatten();
        $this->assertEquals([1, 2, 2, 3, 3, 4, 4, 5], $from1->toMap());
    }

    public function testFirst()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $this->assertEquals(1, $from->first());

    }

    public function testLowest()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);

        $this->assertEquals(1,
            $from->lowest(function ($x) {
                return $x + 1;
            }));
    }

    public function testElementAt()
    {
        $array = [1, 2, 3, 4];
        $from = From::iterable($array);
        $this->assertEquals(3, $from->elementAt(2), 3);

    }
}
